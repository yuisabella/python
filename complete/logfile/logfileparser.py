import pandas as pd
import requests

FILE_NAME = 'nginx_access.log'

SLACK_URL = 'https://hooks.slack.com/services/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'


def post_message_to_slack(text):
    print('sending to slack:')
    print(text)
    #return requests.post(slack_webhook, json.dumps({'text': text}))


def read_file_df():
    df = pd.read_csv(FILE_NAME,
              sep=r'\s(?=(?:[^"]*"[^"]*")*[^"]*$)(?![^\[]*\])',
              engine='python',
              usecols=[0, 3, 4, 5, 6, 7, 8],
              names=['ip', 'time', 'request', 'status', 'size', 'referer', 'user_agent'],
              na_values='-',
              header=None
    )

    return(df)


def main_pandas():
    df = read_file_df()
    not_found = df[ df['status'] == 404]

    post_message_to_slack(not_found['request'].value_counts().head())


def read_file():
    with open(FILE_NAME, 'r') as f:
        return f.readlines()


def get_http_section_and_code(line):
    line = line[line.find('"') + 1: ]

    http_code = line[line.find('"') + 1:].split()[0].strip()

    return line[ :line.find('"')], http_code

def find_most_common(d):
     v = list(d.values())
     k = list(d.keys())
     return k[v.index(max(v))], max(v)

def main_simple():
    log_lines = read_file()

    not_found = {}

    for line in log_lines:
        results = {}
        http_section, code = get_http_section_and_code(line)

        code = int(code)

        if(code == 404):
            if http_section in not_found:
                not_found[http_section] += 1
            else:
                not_found[http_section] = 1

    post_message_to_slack((find_most_common(not_found)))



if __name__ == '__main__':
    main_simple()
    #main_pandas()

