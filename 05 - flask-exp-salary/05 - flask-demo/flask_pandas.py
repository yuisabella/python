import datetime

from flask import Flask
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr

app = Flask(__name__)
api = Api(app)


@api.route('/price/<string:ticker>')
class Price(Resource):
    def get(self, ticker):
        start_date = datetime.datetime.now() - datetime.timedelta(40)
        end_date = datetime.datatime.now()
    

        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
        f['30d mavg'] = df['Adj. Close'].rolling(window=21).mean()
        df['30d std'] = df['Adj. Close'].rolling(window=21).std()
        df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
        df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)
        currentPrice = df.iloc[-1]['Close']
        currentUB =  df.iloc[-1]['Upper Band']
        currentLB =  df.iloc[-1]['Lower Band']
        advice = ""
        if currentPrice >= currentUB:
            advice = "Sell"
        elif currentPrice <= currentLB:
            advice = "Buy"
        else:
            advice = "Hold"

        print(str(df))

        df['Date'] = df.index

        return {'ticker': ticker,
                'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                'price': df.iloc[-1]['Close'],
                'Upper Band': currentUB,
                'Lower Band': currentLB,
                'Advice': advice}

if __name__ == '__main__':
    app.run(debug=True)