import pandas as pd
import requests
import json

LOGFILENAME = 'nginx_access.log'
NEUEDA_URL = 'http://neueda.conygre.com/index.html'
SLACK_WEBHOOK = "https://hooks.slack.com/services/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"


def post_message_to_slack(text):
    return requests.post(SLACK_WEBHOOK, json.dumps({'text': text}))


def get_neueda_site():
    response = requests.get(NEUEDA_URL)
    print(response)


def read_as_df():
    df = pd.read_csv(LOGFILENAME,
              sep=r'\s(?=(?:[^"]*"[^"]*")*[^"]*$)(?![^\[]*\])',
              engine='python',
              usecols=[0, 3, 4, 5, 6, 7, 8],
              names=['ip', 'time', 'request', 'status', 'size', 'referer', 'user_agent'],
              na_values='-',
              header=None
    )

    return(df)


def read_file():
    with open(LOGFILENAME, 'r') as fd:
        lines = fd.readlines()

    return lines


def main():
    print('hello')#
    lines = read_file()

    print('we read ', len(lines), 'lines')